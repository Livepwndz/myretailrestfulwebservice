package com.target.demo.myretailrestfulwebservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles( value = {"local"})
class MyRetailRestfulWebserviceApplicationTests {

	@Test
	void contextLoads() {
	}

}
