package com.target.demo.myretailrestfulwebservice.integration;

import com.target.demo.myretailrestfulwebservice.persistence.ProductPriceTestClass;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( value = {"local"})
class MyRetailMvcTests {
    private static final long TEST_PRODUCT_ID = 13860428;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ProductPriceTestClass productPriceTestClass;

    @BeforeEach
    public void setupTestProduct() {
        productPriceTestClass.setupTestProduct();
    }

    @AfterEach
    public void cleanUpTestProduct() {
        productPriceTestClass.cleanUpTestProduct();
    }


    @Test
    void shouldGetProduct() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/product/{productId}", TEST_PRODUCT_ID))
                .andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
    }



}
