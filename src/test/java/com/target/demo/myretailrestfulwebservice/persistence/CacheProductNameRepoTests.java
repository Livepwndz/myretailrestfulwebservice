package com.target.demo.myretailrestfulwebservice.persistence;

import com.target.demo.myretailrestfulwebservice.persistence.entity.CacheProductName;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles( value = {"local"})
class CacheProductNameRepoTests {

    @Autowired
    private ProductNameTestClass productNameTestClass;

    @BeforeEach
    public void setupTestProduct() {
        productNameTestClass.setupTestProduct();
    }

    @AfterEach
    public void cleanUpTestProduct() {
        productNameTestClass.cleanUpTestProduct();
    }


    @Test
    void shouldGetProductName() {
        Optional<CacheProductName> productPriceO = productNameTestClass.getProductName();
        Assertions.assertTrue(productPriceO.isPresent());
        System.out.println(productPriceO.get());
    }
}
