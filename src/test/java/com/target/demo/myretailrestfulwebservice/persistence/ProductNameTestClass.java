package com.target.demo.myretailrestfulwebservice.persistence;

import com.target.demo.myretailrestfulwebservice.persistence.entity.CacheProductName;
import com.target.demo.myretailrestfulwebservice.persistence.repo.ProductNameRepo;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.util.Optional;

@Component
@ActiveProfiles( value = {"local"})
public class ProductNameTestClass {

    @Autowired
    private ProductNameRepo productNameRepo;

    @Value("${test.product.id}")
    private long testProductId;

    public void setupTestProduct() {
        CacheProductName cacheProductName = new CacheProductName();
        cacheProductName.setProductId(testProductId);
        cacheProductName.setName("The Big Lebowski (Blu-ray)");
        cacheProductName.setCreationTimestamp(Instant.now().toEpochMilli());
        cacheProductName.setLastUpdateTimestamp(Instant.now().toEpochMilli());
        productNameRepo.save(cacheProductName);
    }

    public void cleanUpTestProduct() {
        Optional<CacheProductName> productPriceO = productNameRepo.getByProductId(testProductId);
        Assertions.assertTrue(productPriceO.isPresent());
        productNameRepo.delete(productPriceO.get());
    }

    public Optional<CacheProductName> getProductName() {
        return productNameRepo.getByProductId(testProductId);
    }
}
