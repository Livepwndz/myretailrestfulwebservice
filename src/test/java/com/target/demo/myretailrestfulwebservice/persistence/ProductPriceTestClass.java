package com.target.demo.myretailrestfulwebservice.persistence;

import com.target.demo.myretailrestfulwebservice.persistence.entity.CurrencyCode;
import com.target.demo.myretailrestfulwebservice.persistence.entity.ProductPrice;
import com.target.demo.myretailrestfulwebservice.persistence.repo.ProductPriceRepo;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.util.Optional;

@Component
@ActiveProfiles( value = {"local"})
public class ProductPriceTestClass {

    @Autowired
    private ProductPriceRepo productPriceRepo;

    @Value("${test.product.id}")
    private long testProductId;

    public void setupTestProduct() {
        ProductPrice productPrice = new ProductPrice();
        productPrice.setProductId(testProductId);
        productPrice.setCurrencyCode(CurrencyCode.USD);
        productPrice.setValue(10.99);
        productPrice.setCreationTimestamp(Instant.now().toEpochMilli());
        productPrice.setLastUpdateTimestamp(Instant.now().toEpochMilli());
        productPriceRepo.save(productPrice);
    }

    public void cleanUpTestProduct() {
        Optional<ProductPrice> productPriceO = productPriceRepo.getByProductId(testProductId);
        Assertions.assertTrue(productPriceO.isPresent());
        productPriceRepo.delete(productPriceO.get());
    }

    public Optional<ProductPrice> getProductPrice() {
        return productPriceRepo.getByProductId(testProductId);
    }
}
