package com.target.demo.myretailrestfulwebservice.persistence;

import com.target.demo.myretailrestfulwebservice.persistence.entity.ProductPrice;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles( value = {"local"})
class ProductPriceRepoTests {

    @Autowired
    private ProductPriceTestClass productPriceTestClass;

    @BeforeEach
    public void setupTestProduct() {
        productPriceTestClass.setupTestProduct();
    }

    @AfterEach
    public void cleanUpTestProduct() {
        productPriceTestClass.cleanUpTestProduct();
    }


    @Test
    void shouldGetProductPrice() {
        Optional<ProductPrice> productPriceO = productPriceTestClass.getProductPrice();
        Assertions.assertTrue(productPriceO.isPresent());
        System.out.println(productPriceO.get());
    }
}
