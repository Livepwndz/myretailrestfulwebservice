package com.target.demo.myretailrestfulwebservice.controller;


import com.target.demo.myretailrestfulwebservice.dto.ProductDto;
import com.target.demo.myretailrestfulwebservice.exception.RetailApiException;
import com.target.demo.myretailrestfulwebservice.exception.MyRetailApiNotFoundException;
import com.target.demo.myretailrestfulwebservice.service.ProductService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@ActiveProfiles( value = {"local"})
class ProductControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productNameService;


    @Test( )
    void shouldGet404Product() throws Exception {
        Mockito.when(productNameService.getProductById(Mockito.anyLong())).thenThrow(new MyRetailApiNotFoundException("Not found."));
            mockMvc.perform(MockMvcRequestBuilders.get("/product/{productId}", 1))
                    .andExpect(MockMvcResultMatchers.status().isNotFound());

    }

    @Test( )
    void shouldGet400Product() throws Exception {
        Mockito.when(productNameService.getProductById(Mockito.anyLong())).thenThrow(new RetailApiException("Error getting product."));
        mockMvc.perform(MockMvcRequestBuilders.get("/product/{productId}", 1))
                .andExpect(MockMvcResultMatchers.status().is(400));

    }

    @Test( )
    void shouldGet500Product() throws Exception {
        Mockito.when(productNameService.getProductById(Mockito.anyLong())).thenThrow(new RuntimeException("Error getting product."));
        mockMvc.perform(MockMvcRequestBuilders.get("/product/{productId}", 1))
                .andExpect(MockMvcResultMatchers.status().is(500));

    }


    @Test
    void shouldGetProduct() throws Exception {
        Mockito.when(productNameService.getProductById(Mockito.anyLong())).thenReturn(new ProductDto());
        mockMvc.perform(MockMvcRequestBuilders.get("/product/{productId}", 1))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}
