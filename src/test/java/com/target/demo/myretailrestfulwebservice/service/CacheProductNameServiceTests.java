package com.target.demo.myretailrestfulwebservice.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest( classes = {InMemoryProductNameService.class})
@ActiveProfiles( value = {"local"})
class CacheProductNameServiceTests {

    @Autowired
    private ProductNameService productNameService;

    @Test
    void shouldGetProductName() {
        Optional<String> nameO = productNameService.getProductNameById(13860428);
        Assertions.assertFalse(nameO.isEmpty());
        String TEST_PRODUCT_NAME = "LG 50' LCD TV Screen";
        Assertions.assertEquals(TEST_PRODUCT_NAME, nameO.get());
    }


    @Test
    void shouldNotGetProductName() {
        Optional<String> nameO = productNameService.getProductNameById(-1);
        Assertions.assertTrue(nameO.isEmpty());
    }

}
