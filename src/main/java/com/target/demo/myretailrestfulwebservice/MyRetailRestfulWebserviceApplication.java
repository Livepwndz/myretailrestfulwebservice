package com.target.demo.myretailrestfulwebservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyRetailRestfulWebserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyRetailRestfulWebserviceApplication.class, args);
	}

}
