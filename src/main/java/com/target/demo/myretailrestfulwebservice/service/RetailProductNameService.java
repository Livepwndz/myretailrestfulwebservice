package com.target.demo.myretailrestfulwebservice.service;

import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.target.demo.myretailrestfulwebservice.exception.RetailApiException;
import com.target.demo.myretailrestfulwebservice.persistence.entity.CacheProductName;
import com.target.demo.myretailrestfulwebservice.persistence.repo.AmazonDynamoDbProductNameCacheRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Primary;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

@Service
@Primary
public class RetailProductNameService implements ProductNameService {
    private final String serviceUrl;
    private final String productFilters;
    private final long productNameMaxTtlInMilli;

    private static final Logger LOG = LoggerFactory.getLogger(RetailProductNameService.class);
    private final JsonPointer productNameJsonPointer;

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;
    private final AmazonDynamoDbProductNameCacheRepo productNameCacheRepo;


    public RetailProductNameService(@Value("${product.name.service.url}") String productNameServiceUrl
            , @Value("${product.filters}") String productFilters
            , @Value("${product.name.max.ttl.milli}") long productNameMaxTtlInMilli
            , @Value("${product.name.json.path}") String productNameJsonPath
            , RestTemplateBuilder builder, ObjectMapper objectMapper, AmazonDynamoDbProductNameCacheRepo productNameCacheRepo) {

        serviceUrl = productNameServiceUrl;
        this.productFilters = productFilters;
        this.productNameMaxTtlInMilli = productNameMaxTtlInMilli;
        restTemplate = builder.build();
        this.objectMapper = objectMapper;
        this.productNameCacheRepo = productNameCacheRepo;

        productNameJsonPointer = JsonPointer.compile(productNameJsonPath);
    }


    @Override
    public Optional<String> getProductNameById(long productId) {
        Optional<CacheProductName> productNameO = productNameCacheRepo.getByProductId(productId);
        if (productNameO.isPresent() && isCacheValid(productNameO.get()))
            return Optional.of(productNameO.get().getName());
        return fetchAndUpdateCache(productId);
    }

    private Optional<String> fetchAndUpdateCache(long productId) {
        Optional<String> nameO = fetchProductName(productId);
        if (nameO.isEmpty()) return nameO;

        CacheProductName cacheProductName = new CacheProductName();
        cacheProductName.setProductId(productId);
        cacheProductName.setName(nameO.get());
        cacheProductName.setLastUpdateTimestamp(Instant.now().toEpochMilli());
        productNameCacheRepo.save(cacheProductName);
        return nameO;

    }

    private boolean isCacheValid(CacheProductName productName) {
        long lastUpdateTimestamp = productName.getLastUpdateTimestamp();
        long currentTimestamp = Instant.now().toEpochMilli();
        return (currentTimestamp - lastUpdateTimestamp <= productNameMaxTtlInMilli);
    }

    private Optional<String> fetchProductName(long productId) {
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(String.format(serviceUrl, productFilters), String.class, productId);
        if (!responseEntity.getStatusCode().is2xxSuccessful()) {
            throw new RetailApiException("Request for name failed.");
        }

        try {
            String response = responseEntity.getBody();
            LOG.info("Product Name response: {}", response);
            JsonNode root = objectMapper.readTree(Objects.requireNonNull(response));
            return Optional.ofNullable(root.at(productNameJsonPointer).asText());
        } catch (Exception ex) {
            LOG.error("Product name service error.", ex);
            throw new RetailApiException("Error occured while extracting json.");
        }
    }

}
