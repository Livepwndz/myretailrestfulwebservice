package com.target.demo.myretailrestfulwebservice.service;

import com.target.demo.myretailrestfulwebservice.dto.ProductDto;

public interface ProductService {
    ProductDto getProductById(long productId);
}
