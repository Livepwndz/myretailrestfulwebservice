package com.target.demo.myretailrestfulwebservice.service;

import com.target.demo.myretailrestfulwebservice.persistence.entity.ProductPrice;
import com.target.demo.myretailrestfulwebservice.persistence.repo.ProductPriceRepo;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RetailProductPriceService implements ProductPriceService {
    private final ProductPriceRepo productPriceRepo;

    public RetailProductPriceService(ProductPriceRepo productPriceRepo) {
        this.productPriceRepo = productPriceRepo;
    }

    @Override
    public Optional<ProductPrice> getProductPriceById(long productId) {
        return productPriceRepo.getByProductId(productId);
    }
}
