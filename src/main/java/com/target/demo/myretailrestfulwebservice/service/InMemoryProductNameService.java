package com.target.demo.myretailrestfulwebservice.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class InMemoryProductNameService implements ProductNameService {
    private static Map<Long, String> productNameMap = new HashMap<>();

    static {
        productNameMap.put(13860428L, "LG 50' LCD TV Screen");
    }

    @Override
    public Optional<String> getProductNameById(long productId) {
        if(productNameMap.containsKey(productId)){
            return Optional.of(productNameMap.get(productId));
        }

        return Optional.empty();
    }
}
