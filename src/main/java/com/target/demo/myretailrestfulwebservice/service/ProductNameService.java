package com.target.demo.myretailrestfulwebservice.service;

import java.util.Optional;

public interface ProductNameService {
    Optional<String> getProductNameById(long productId);
}
