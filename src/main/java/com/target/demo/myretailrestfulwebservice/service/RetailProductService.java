package com.target.demo.myretailrestfulwebservice.service;

import com.target.demo.myretailrestfulwebservice.dto.ProductDto;
import com.target.demo.myretailrestfulwebservice.dto.ProductPriceDto;
import com.target.demo.myretailrestfulwebservice.exception.RetailApiException;
import com.target.demo.myretailrestfulwebservice.exception.MyRetailApiNotFoundException;
import com.target.demo.myretailrestfulwebservice.persistence.entity.ProductPrice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@Service
public class RetailProductService implements ProductService {
    private final ExecutorService executorService = Executors.newCachedThreadPool();
    private final ProductNameService productNameService;
    private final ProductPriceService productPriceService;

    private static final Logger LOG = LoggerFactory.getLogger(RetailProductService.class);
    private final long productNameRequestTimeoutInMilli;

    public RetailProductService(ProductNameService productNameService
            , ProductPriceService productPriceService
            , @Value("${product.name.request.timeout.milli}") long productNameRequestTimeoutInMilli) {
        this.productNameService = productNameService;
        this.productPriceService = productPriceService;
        this.productNameRequestTimeoutInMilli = productNameRequestTimeoutInMilli;
    }

    @Override
    public ProductDto getProductById(long productId) {
        Future<Optional<String>> futureProductName = executorService.submit(() -> {
            return productNameService.getProductNameById(productId);
        });

        Optional<ProductPrice> priceO = productPriceService.getProductPriceById(productId);
        Optional<String> nameO = extractProductName(futureProductName);

        if(nameO.isEmpty()){
            throw new MyRetailApiNotFoundException("Product id "+productId+" Name not found.");
        }

        if(priceO.isEmpty()){
            throw new MyRetailApiNotFoundException("Product id "+productId+" Price not found.");
        }

        ProductPrice productPrice = priceO.get();
        ProductPriceDto productPriceDto = new ProductPriceDto();
        productPriceDto.setValue(BigDecimal.valueOf(productPrice.getValue()));
        productPriceDto.setCurrencyCode(productPrice.getCurrencyCode());

        ProductDto productDto = new ProductDto();
        productDto.setProductId(productPrice.getProductId());
        productDto.setName(nameO.get());
        productDto.setCurrentPrice(productPriceDto);

        return productDto;
    }

    private Optional<String> extractProductName(Future<Optional<String>> futureProductName) {
        try {
            return futureProductName.get(productNameRequestTimeoutInMilli, TimeUnit.MILLISECONDS);
        } catch (Exception ex) {
            LOG.error("Product Name Extraction Error", ex);
            throw new RetailApiException("Could not fetch product name.");
        }
    }





}
