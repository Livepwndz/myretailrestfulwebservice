package com.target.demo.myretailrestfulwebservice.service;

import com.target.demo.myretailrestfulwebservice.persistence.entity.ProductPrice;

import java.util.Optional;

public interface ProductPriceService {
    Optional<ProductPrice> getProductPriceById(long productPrice);
}
