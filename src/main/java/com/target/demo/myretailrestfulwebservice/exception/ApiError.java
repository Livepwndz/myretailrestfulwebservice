package com.target.demo.myretailrestfulwebservice.exception;

public class ApiError {
	private String msg;

	public ApiError(String msg ) {
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
}
