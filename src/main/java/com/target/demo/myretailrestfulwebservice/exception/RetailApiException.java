package com.target.demo.myretailrestfulwebservice.exception;

public class RetailApiException extends RuntimeException {
    public RetailApiException(String msg) {
        super(msg);
    }
}
