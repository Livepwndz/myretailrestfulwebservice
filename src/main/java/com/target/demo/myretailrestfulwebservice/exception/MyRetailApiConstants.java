package com.target.demo.myretailrestfulwebservice.exception;

public class MyRetailApiConstants {

    public static final String APP_FATAL_ERROR_MSG = "Api fatal error occured. Please check logs for details.";

    private MyRetailApiConstants(){
    }
}
