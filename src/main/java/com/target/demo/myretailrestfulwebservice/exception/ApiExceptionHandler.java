package com.target.demo.myretailrestfulwebservice.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {
    private static final String ERROR_TAG = "Api error" ;
    private static final Logger LOG = LoggerFactory.getLogger(ApiExceptionHandler.class);


    @ExceptionHandler({MyRetailApiNotFoundException.class})
    public ResponseEntity<ApiError> handleResourceExceptions(MyRetailApiNotFoundException exception) {
        LOG.error(ERROR_TAG, exception);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiError(exception.getMessage()));
    }

    @ExceptionHandler({RetailApiException.class})
    public ResponseEntity<ApiError> handleResourceExceptions(RetailApiException exception) {
        LOG.error(ERROR_TAG, exception);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError(exception.getMessage()));
    }


    @ExceptionHandler({Exception.class})
    public ResponseEntity<ApiError> handleResourceExceptions(Exception exception) {
        LOG.error(ERROR_TAG, exception);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ApiError(exception.getMessage()));
    }
}
