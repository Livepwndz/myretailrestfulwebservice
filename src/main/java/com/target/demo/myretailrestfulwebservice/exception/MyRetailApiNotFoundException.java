package com.target.demo.myretailrestfulwebservice.exception;

public class MyRetailApiNotFoundException extends RuntimeException {
    public MyRetailApiNotFoundException(String msg) {
        super(msg);
    }
}
