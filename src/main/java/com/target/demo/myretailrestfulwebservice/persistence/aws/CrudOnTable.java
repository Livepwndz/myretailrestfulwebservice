package com.target.demo.myretailrestfulwebservice.persistence.aws;

import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
public class CrudOnTable<A> {
    protected final ProductDynamoDBMapperProvider mapperProvider;

    protected CrudOnTable(ProductDynamoDBMapperProvider mapperProvider) {
        this.mapperProvider = mapperProvider;
    }

    public void save(A a) {
        mapperProvider.getDbMapper().save(a);
    }

    public void delete(A a) {
        mapperProvider.getDbMapper().delete(a);
    }

    public Optional<A> loadByPartitionAndSortKey(Class<A> aClass, String partitionKeyValue, String sortKeyValue) {
        return Optional.ofNullable(mapperProvider.getDbMapper().load(aClass, partitionKeyValue, sortKeyValue));
    }
}
