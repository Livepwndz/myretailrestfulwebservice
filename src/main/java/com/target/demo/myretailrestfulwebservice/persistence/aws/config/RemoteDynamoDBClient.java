package com.target.demo.myretailrestfulwebservice.persistence.aws.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile( value = {"uat"})
public class RemoteDynamoDBClient implements DynamoDBClient {
    @Override
    public AmazonDynamoDB getAmazonDynamoDb() {
        return AmazonDynamoDBClientBuilder.standard()
                .withRegion(Regions.fromName(Regions.EU_CENTRAL_1.getName()))
                .withCredentials(new AWSStaticCredentialsProvider(getAwsRemoteCreds()))
                .build();
    }

    private AWSCredentials getAwsRemoteCreds() {
        String accessKey = "no-creds-access";
        String secretKey = "no-creds-secret";
        return new BasicAWSCredentials(accessKey, secretKey );
    }
}
