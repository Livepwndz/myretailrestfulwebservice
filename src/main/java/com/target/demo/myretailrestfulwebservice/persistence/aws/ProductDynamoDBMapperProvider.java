package com.target.demo.myretailrestfulwebservice.persistence.aws;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.target.demo.myretailrestfulwebservice.persistence.aws.config.DynamoDBClient;
import org.springframework.stereotype.Component;


@Component
public class ProductDynamoDBMapperProvider {

    private final DynamoDBClient dbClient;

    private DynamoDBMapper dbMapper;

    public ProductDynamoDBMapperProvider(DynamoDBClient dbClient) {
        this.dbClient = dbClient;
    }

    public DynamoDBMapper getDbMapper() {
        if (dbMapper == null) {
            dbMapper = new DynamoDBMapper(dbClient.getAmazonDynamoDb());
        }
        return dbMapper;
    }


}
