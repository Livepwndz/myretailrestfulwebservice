package com.target.demo.myretailrestfulwebservice.persistence.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.target.demo.myretailrestfulwebservice.persistence.aws.DynamoConstants;

/**
 * @author 3y3r
 **/

@DynamoDBTable(tableName = DynamoConstants.PRODUCT_TABLE_NAME)
public class CacheProductName {
    private String nameDataId;
    private String productDataCategoryId;
    private long productId;
    private String name;
    private long creationTimestamp;
    private long lastUpdateTimestamp;


    @DynamoDBHashKey(attributeName = DynamoConstants.TABLE_PARTITION_KEY_NAME)
    public String getNameDataId() {
        return nameDataId;
    }

    public void setNameDataId(String nameDataId) {
        this.nameDataId = nameDataId;
    }

    @DynamoDBRangeKey(attributeName = DynamoConstants.TABLE_SORT_KEY_NAME)
    public String getProductDataCategoryId() {
        return productDataCategoryId;
    }

    public void setProductDataCategoryId(String productDataCategoryId) {
        this.productDataCategoryId = productDataCategoryId;
    }

    @DynamoDBAttribute(attributeName = DynamoConstants.PRODUCT_ID)
    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    @DynamoDBAttribute(attributeName = DynamoConstants.PRICE_AMOUNT)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @DynamoDBAttribute(attributeName = DynamoConstants.PRODUCT_CREATION_TIMESTAMP)
    public long getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(long creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    @DynamoDBAttribute(attributeName = DynamoConstants.PRODUCT_LAST_UPDATE_TIMESTAMP)
    public long getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    public void setLastUpdateTimestamp(long lastUpdateTimestamp) {
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }

    @Override
    public String toString() {
        return "ProductPrice{" +
                "priceDataId='" + nameDataId + '\'' +
                ", productDataCategoryId='" + productDataCategoryId + '\'' +
                ", productId=" + productId +
                ", name=" + name +
                ", creationTimestamp=" + creationTimestamp +
                ", lastUpdateTimestamp=" + lastUpdateTimestamp +
                '}';
    }
}
