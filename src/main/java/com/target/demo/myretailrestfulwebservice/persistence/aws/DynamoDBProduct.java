package com.target.demo.myretailrestfulwebservice.persistence.aws;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.dynamodbv2.util.TableUtils;
import com.target.demo.myretailrestfulwebservice.exception.MyRetailApiConstants;
import com.target.demo.myretailrestfulwebservice.exception.RetailApiException;
import com.target.demo.myretailrestfulwebservice.persistence.aws.config.DynamoDBClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.target.demo.myretailrestfulwebservice.persistence.aws.DynamoConstants.*;


@Component
public class DynamoDBProduct {
    private static final Logger LOG = LoggerFactory.getLogger(DynamoDBProduct.class.getName());
    private final DynamoDBClient dynamoDBClient;
    public DynamoDBProduct(DynamoDBClient dynamoDBClient){
        this.dynamoDBClient = dynamoDBClient;
        createTableIfNotExist();
    }

    private void createTableIfNotExist() {
        AmazonDynamoDB dbClient = dynamoDBClient.getAmazonDynamoDb();
        LOG.info("Does PRODUCT dynamo db table exist?...");
        if (!doesTableExist(dbClient)) {
            LOG.info("PRODUCT dynamo db table missing. Now creating...");
            dbClient.createTable(getCreateTableRequest());
            try {
                TableUtils.waitUntilActive(dbClient, PRODUCT_TABLE_NAME);
                LOG.info("PRODUCT dynamo db table created with name: {}", PRODUCT_TABLE_NAME);
            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
                throw new RetailApiException(MyRetailApiConstants.APP_FATAL_ERROR_MSG);
            }
        }
        LOG.info("PRODUCT dynamo db table exists with name: {}", PRODUCT_TABLE_NAME);
    }


    private CreateTableRequest getCreateTableRequest() {
        KeySchemaElement tablePartitionKeySchema = new KeySchemaElement(TABLE_PARTITION_KEY_NAME, KeyType.HASH);
        KeySchemaElement tableSortKeySchema = new KeySchemaElement(TABLE_SORT_KEY_NAME, KeyType.RANGE);


        List<KeySchemaElement> tableKeySchemaElements = new ArrayList<>();
        tableKeySchemaElements.add(tablePartitionKeySchema);
        tableKeySchemaElements.add(tableSortKeySchema);

        AttributeDefinition tablePartitionKey = new AttributeDefinition(TABLE_PARTITION_KEY_NAME, ScalarAttributeType.S);
        AttributeDefinition tableSortKey = new AttributeDefinition(TABLE_SORT_KEY_NAME, ScalarAttributeType.S);

        List<AttributeDefinition> attributeDefinitions = new ArrayList<>();
        attributeDefinitions.add(tablePartitionKey);
        attributeDefinitions.add(tableSortKey);


        return new CreateTableRequest()
                .withAttributeDefinitions(attributeDefinitions)
                .withKeySchema(tableKeySchemaElements)
                .withProvisionedThroughput(new ProvisionedThroughput(
                        10L, 10L))
                .withTableName(PRODUCT_TABLE_NAME);
    }

    private boolean doesTableExist(AmazonDynamoDB ddbClient) {
        try {
            ddbClient.describeTable(PRODUCT_TABLE_NAME);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            return false;
        }

        return true;

    }
}
