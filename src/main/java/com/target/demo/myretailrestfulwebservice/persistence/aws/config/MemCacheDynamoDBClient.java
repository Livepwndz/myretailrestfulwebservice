package com.target.demo.myretailrestfulwebservice.persistence.aws.config;

import com.amazon.dax.client.dynamodbv2.AmazonDaxClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile( value = {"production"})
public class MemCacheDynamoDBClient implements DynamoDBClient {
    @Override
    public AmazonDynamoDB getAmazonDynamoDb() {
        AmazonDaxClientBuilder daxClientBuilder = AmazonDaxClientBuilder.standard();
        daxClientBuilder.withRegion(Regions.EU_CENTRAL_1.getName()).withEndpointConfiguration("mydaxcluster.2cmrwl.clustercfg.dax.use1.cache.amazonaws.com:8111");
        return daxClientBuilder.build();
    }
}
