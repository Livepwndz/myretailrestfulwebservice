package com.target.demo.myretailrestfulwebservice.persistence.repo;

import com.target.demo.myretailrestfulwebservice.persistence.aws.CrudOnTable;
import com.target.demo.myretailrestfulwebservice.persistence.aws.DynamoConstants;
import com.target.demo.myretailrestfulwebservice.persistence.entity.ProductPrice;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
public class AmazonDynamoDbProductPriceRepo implements ProductPriceRepo {

    private final CrudOnTable<ProductPrice> crudOnTable;

    public AmazonDynamoDbProductPriceRepo(CrudOnTable<ProductPrice> crudOnTable) {
        this.crudOnTable = crudOnTable;
    }

    @Override
    public Optional<ProductPrice> getByProductId(long productId) {
        return crudOnTable.loadByPartitionAndSortKey(ProductPrice.class,resolvePartitionKey(productId), DynamoConstants.PRODUCT_PRICE_DATA );
    }

    private String resolvePartitionKey(long productId) {
        return DynamoConstants.PRODUCT_PRICE_DATA + "-" + productId;
    }


    @Override
    public void save(ProductPrice productPrice) {
        productPrice.setPriceDataId(resolvePartitionKey(productPrice.getProductId()));
        productPrice.setProductDataCategoryId(DynamoConstants.PRODUCT_PRICE_DATA);
        crudOnTable.save(productPrice);
    }

    @Override
    public void delete(ProductPrice productPrice) {
        productPrice.setPriceDataId(resolvePartitionKey(productPrice.getProductId()));
        productPrice.setProductDataCategoryId(DynamoConstants.PRODUCT_PRICE_DATA);
        crudOnTable.delete(productPrice);
    }


}
