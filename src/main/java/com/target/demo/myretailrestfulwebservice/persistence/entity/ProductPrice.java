package com.target.demo.myretailrestfulwebservice.persistence.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.target.demo.myretailrestfulwebservice.persistence.aws.DynamoConstants;

/**
 * @author 3y3r
 **/

@DynamoDBTable(tableName = DynamoConstants.PRODUCT_TABLE_NAME)
public class ProductPrice {
    private String priceDataId;
    private String productDataCategoryId;
    private long productId;
    private double value;
    private CurrencyCode currencyCode;
    private long creationTimestamp;
    private long lastUpdateTimestamp;


    @DynamoDBHashKey(attributeName = DynamoConstants.TABLE_PARTITION_KEY_NAME)
    public String getPriceDataId() {
        return priceDataId;
    }

    public void setPriceDataId(String priceDataId) {
        this.priceDataId = priceDataId;
    }

    @DynamoDBRangeKey(attributeName = DynamoConstants.TABLE_SORT_KEY_NAME)
    public String getProductDataCategoryId() {
        return productDataCategoryId;
    }

    public void setProductDataCategoryId(String productDataCategoryId) {
        this.productDataCategoryId = productDataCategoryId;
    }

    @DynamoDBAttribute(attributeName = DynamoConstants.PRODUCT_ID)
    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    @DynamoDBAttribute(attributeName = DynamoConstants.PRICE_AMOUNT)
    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @DynamoDBTyped(value = DynamoDBMapperFieldModel.DynamoDBAttributeType.S)
    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }
    public void setCurrencyCode(CurrencyCode currencyCode) {
        this.currencyCode = currencyCode;
    }

    @DynamoDBAttribute(attributeName = DynamoConstants.PRODUCT_CREATION_TIMESTAMP)
    public long getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(long creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    @DynamoDBAttribute(attributeName = DynamoConstants.PRODUCT_LAST_UPDATE_TIMESTAMP)
    public long getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    public void setLastUpdateTimestamp(long lastUpdateTimestamp) {
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }

    @Override
    public String toString() {
        return "ProductPrice{" +
                "priceDataId='" + priceDataId + '\'' +
                ", productDataCategoryId='" + productDataCategoryId + '\'' +
                ", productId=" + productId +
                ", value=" + value +
                ", currencyCode=" + currencyCode +
                ", creationTimestamp=" + creationTimestamp +
                ", lastUpdateTimestamp=" + lastUpdateTimestamp +
                '}';
    }
}
