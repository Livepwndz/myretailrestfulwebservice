package com.target.demo.myretailrestfulwebservice.persistence.aws.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile( value = {"local"})
public class LocalDynamoDBClient implements DynamoDBClient {
    @Override
    public AmazonDynamoDB getAmazonDynamoDb() {
        AwsClientBuilder.EndpointConfiguration endpointConfig = new AwsClientBuilder.EndpointConfiguration("http://localhost:8000", Regions.EU_CENTRAL_1.getName());
        AmazonDynamoDBClientBuilder builder = AmazonDynamoDBClientBuilder.standard();
        builder.setEndpointConfiguration(endpointConfig);
        builder.setCredentials(getAwsLocalCredsProvider());
        return builder.build();
    }

    private AWSCredentialsProvider getAwsLocalCredsProvider() {
        return new AWSCredentialsProvider() {
            @Override
            public AWSCredentials getCredentials() {
                return new AWSCredentials() {
                    @Override
                    public String getAWSAccessKeyId() {
                        return "sample";
                    }

                    @Override
                    public String getAWSSecretKey() {
                        return "sample";
                    }
                };
            }

            @Override
            public void refresh() {
                //Not relevant on local dynamo.
            }
        };
    }
}
