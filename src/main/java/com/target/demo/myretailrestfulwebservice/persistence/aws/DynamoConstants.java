package com.target.demo.myretailrestfulwebservice.persistence.aws;

public class DynamoConstants {

    public static final String PRODUCT_TABLE_NAME = "PRODUCT_SCHEMA_v1";
    public static final String TABLE_PARTITION_KEY_NAME = "table_partition_id";
    public static final String TABLE_SORT_KEY_NAME = "table_sort_id";
    public static final String PRODUCT_CREATION_TIMESTAMP = "product_creation_timestamp";
    public static final String PRODUCT_LAST_UPDATE_TIMESTAMP = "product_last_update_timestamp";
    public static final String PRICE_AMOUNT = "product_price_amount";
    public static final String PRODUCT_PRICE_DATA = "product_price_data";
    public static final String PRODUCT_ID = "product_id";
    public static final String PRODUCT_NAME_DATA = "product_name_data";

    private DynamoConstants(){
    }
}
