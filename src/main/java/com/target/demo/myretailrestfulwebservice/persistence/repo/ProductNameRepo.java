package com.target.demo.myretailrestfulwebservice.persistence.repo;

import com.target.demo.myretailrestfulwebservice.persistence.entity.CacheProductName;

import java.util.Optional;

public interface ProductNameRepo {
    Optional<CacheProductName> getByProductId(long productId);
    void save(CacheProductName cacheProductName);
    void delete(CacheProductName cacheProductName);
}
