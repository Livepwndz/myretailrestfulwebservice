package com.target.demo.myretailrestfulwebservice.persistence.repo;

import com.target.demo.myretailrestfulwebservice.persistence.entity.ProductPrice;

import java.util.Optional;

public interface ProductPriceRepo {
    Optional<ProductPrice> getByProductId(long productId);
    void save(ProductPrice productPrice);
    void delete(ProductPrice productPrice);
}
