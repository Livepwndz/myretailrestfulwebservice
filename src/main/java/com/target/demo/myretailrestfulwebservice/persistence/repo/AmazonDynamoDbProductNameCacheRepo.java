package com.target.demo.myretailrestfulwebservice.persistence.repo;

import com.target.demo.myretailrestfulwebservice.persistence.aws.CrudOnTable;
import com.target.demo.myretailrestfulwebservice.persistence.aws.DynamoConstants;
import com.target.demo.myretailrestfulwebservice.persistence.entity.CacheProductName;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
public class AmazonDynamoDbProductNameCacheRepo implements ProductNameRepo {

    private final CrudOnTable<CacheProductName> crudOnTable;

    public AmazonDynamoDbProductNameCacheRepo(CrudOnTable<CacheProductName> crudOnTable) {
        this.crudOnTable = crudOnTable;
    }

    @Override
    public Optional<CacheProductName> getByProductId(long productId) {
        return crudOnTable.loadByPartitionAndSortKey(CacheProductName.class,resolvePartitionKey(productId), DynamoConstants.PRODUCT_NAME_DATA );
    }

    private String resolvePartitionKey(long productId) {
        return DynamoConstants.PRODUCT_NAME_DATA + "-" + productId;
    }


    @Override
    public void save(CacheProductName cacheProductName) {
        cacheProductName.setNameDataId(resolvePartitionKey(cacheProductName.getProductId()));
        cacheProductName.setProductDataCategoryId(DynamoConstants.PRODUCT_NAME_DATA);
        crudOnTable.save(cacheProductName);
    }

    @Override
    public void delete(CacheProductName cacheProductName) {
        cacheProductName.setNameDataId(resolvePartitionKey(cacheProductName.getProductId()));
        cacheProductName.setProductDataCategoryId(DynamoConstants.PRODUCT_NAME_DATA);
        crudOnTable.delete(cacheProductName);
    }


}
