package com.target.demo.myretailrestfulwebservice.persistence.aws.config;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;

public interface DynamoDBClient {
    AmazonDynamoDB getAmazonDynamoDb();
}
