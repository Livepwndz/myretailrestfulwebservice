package com.target.demo.myretailrestfulwebservice.dto;

import com.target.demo.myretailrestfulwebservice.persistence.entity.CurrencyCode;

import java.math.BigDecimal;

public class ProductPriceDto {
    private BigDecimal value;
    private CurrencyCode currencyCode;

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(CurrencyCode currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Override
    public String toString() {
        return "ProductPriceDto{" +
                "value=" + value +
                ", currencyCode=" + currencyCode +
                '}';
    }
}
