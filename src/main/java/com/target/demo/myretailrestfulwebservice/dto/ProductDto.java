package com.target.demo.myretailrestfulwebservice.dto;

public class ProductDto {
    private long productId;
    private String name;
    private ProductPriceDto currentPrice;

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductPriceDto getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(ProductPriceDto currentPrice) {
        this.currentPrice = currentPrice;
    }

    @Override
    public String toString() {
        return "ProductDto{" +
                "productId=" + productId +
                ", name='" + name + '\'' +
                ", currentPrice=" + currentPrice +
                '}';
    }
}
