package com.target.demo.myretailrestfulwebservice.controller;

import com.target.demo.myretailrestfulwebservice.dto.ProductDto;
import com.target.demo.myretailrestfulwebservice.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping( value = "/product")
public class ProductController {

    private final ProductService productNameService;

    public ProductController(ProductService productNameService) {
        this.productNameService = productNameService;
    }

    @GetMapping( value = "/{productId}")
    public ResponseEntity<ProductDto> getProduct(@PathVariable Long productId){
        ProductDto productDto = productNameService.getProductById(productId);
        return ResponseEntity.ok(productDto);
    }
}
